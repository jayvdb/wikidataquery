CC=gcc
CXX=g++
CPPFLAGS=-g -pthread

SRCS_C=mongoose.c
SRCS_CXX_TOOL= myjson.cpp wd_inmem.cpp TItemQuery.cpp  wd_tool.cpp
SRCS_CXX_SERVER= myjson.cpp wd_inmem.cpp TItemQuery.cpp  wd_server.cpp
SRCS_CXX= $(SRCS_CXX_TOOL) $(SRCS_CXX_SERVER)
# wd_server.cpp 
# block_allocator.cpp json.cpp 

#EXE=wd_tool
#wd_inmem


OBJS_TOOL=$(subst .cpp,.o,$(SRCS_CXX_TOOL))
OBJS_SERVER=$(subst .c,.o,$(SRCS_C)) $(subst .cpp,.o,$(SRCS_CXX_SERVER))
OBJS=$(OBJS_TOOL) $(OBJS_SERVER)

all:
	make wd_tool
	make wd_server

wd_tool: $(OBJS_TOOL)
	g++ $(OBJS_TOOL) -o wd_tool -O3 $(CPPFLAGS) -ldl

wd_server: $(OBJS_SERVER)
	g++ $(OBJS_SERVER) -o wd_server -O3 $(CPPFLAGS) -ldl

clean:
	-rm $(OBJS) wd_tool wd_server
	-rm -rf test-data

%.o: %.cpp
	g++ $(CPPFLAGS) -c $<

WD_TOOL=./wd_tool

TESTS = test_claim_valid test_claim_invalid test_noclaim_valid test_noclaim_invalid test_string_valid test_string_invalid test_quantity_valid test_quantity_invalid test_between_valid test_between_invalid test_claim_qualifier_between_valid test_claim_qualifier_between_invalid test_claim_qualifier_claim_valid test_claim_qualifier_claim_invalid test_claim_qualifier_claim_and_noclaim_valid test_claim_qualifier_claim_and_noclaim_invalid

tests: wd_tool $(TESTS)

test-data/dir:
	mkdir -p test-data
	touch test-data/dir

test-data/Q60.xml: test-data/dir
	wget -O test-data/Q60.xml "http://www.wikidata.org/wiki/Special:Export/Q60"

test-data/Q%.bin: test-data/Q%.xml $(WD_TOOL)
	cat $< | $(WD_TOOL) dump2bin > $@

test-data/result-none: test-data/dir
	echo '' > test-data/result-none

test-data/result-60: test-data/dir
	echo '60' > test-data/result-60

test_claim_valid: test-data/Q60.bin test-data/result-60
	$(WD_TOOL) query $< 'claim[6:4911497]' 2>/dev/null | cmp - test-data/result-60

test_claim_invalid: test-data/Q60.bin test-data/result-none
	$(WD_TOOL) query $< 'claim[6:4911498]' 2>/dev/null | cmp - test-data/result-none

test_noclaim_valid: test-data/Q60.bin test-data/result-60
	$(WD_TOOL) query $< 'claim[6:4911497] and noclaim[6:4911498]' 2>/dev/null | cmp - test-data/result-60

test_noclaim_invalid: test-data/Q60.bin test-data/result-none
	$(WD_TOOL) query $< 'claim[6:4911497] and noclaim[6:4911497]' 2>/dev/null | cmp - test-data/result-none

test_string_valid: test-data/Q60.bin test-data/result-60
	$(WD_TOOL) query $< 'string[213:"0000 0001 2196 6726"]' 2>/dev/null | cmp - test-data/result-60

test_string_invalid: test-data/Q60.bin test-data/result-none
	$(WD_TOOL) query $< 'string[213:"0000 0001 2196 6727"]' 2>/dev/null | cmp - test-data/result-none

test_quantity_valid: test-data/Q60.bin test-data/result-60
	$(WD_TOOL) query $< 'quantity[1082,4937,4937]' 2>/dev/null | cmp - test-data/result-60

test_quantity_invalid: test-data/Q60.bin test-data/result-none
	$(WD_TOOL) query $< 'quantity[1082,4938,4938]' 2>/dev/null | cmp - test-data/result-none

test_between_valid: test-data/Q60.bin test-data/result-60
	$(WD_TOOL) query $< 'between[571,1623,1625]' 2>/dev/null | cmp - test-data/result-60

test_between_invalid: test-data/Q60.bin test-data/result-none
	$(WD_TOOL) query $< 'between[571,1625,1626]' 2>/dev/null | cmp - test-data/result-none

test_claim_qualifier_between_valid: test-data/Q60.bin test-data/result-60
	$(WD_TOOL) query $< 'claim[6:4911497]{between[580,+00000002013-12-32T00:00:00Z,+00000002014-01-02T00:00:00Z]}' 2>/dev/null | cmp - test-data/result-60

test_claim_qualifier_between_invalid: test-data/Q60.bin test-data/result-none
	$(WD_TOOL) query $< 'claim[6:4911497]{between[580,+00000002014-01-02T00:00:00Z,+00000002014-01-03T00:00:00Z]}' 2>/dev/null | cmp - test-data/result-none

test_claim_qualifier_claim_valid: test-data/Q60.bin test-data/result-60
	$(WD_TOOL) query $< 'claim[6:939458]{claim[39:13436054]}' 2>/dev/null | cmp - test-data/result-60

test_claim_qualifier_claim_invalid: test-data/Q60.bin test-data/result-none
	$(WD_TOOL) query $< 'claim[6:939458]{claim[39:13436055]}' 2>/dev/null | cmp - test-data/result-none

test_claim_qualifier_claim_and_noclaim_valid: test-data/Q60.bin test-data/result-60
	$(WD_TOOL) query $< 'claim[6:939458]{claim[39:13436054] and noclaim[39:13436055]}' 2>/dev/null | cmp - test-data/result-60

test_claim_qualifier_claim_and_noclaim_invalid: test-data/Q60.bin test-data/result-none
	$(WD_TOOL) query $< 'claim[6:939458]{claim[39:13436054] and noclaim[39:13436054]}' 2>/dev/null | cmp - test-data/result-none
