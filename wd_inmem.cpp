#include "wd_inmem.h"
/*
template <class T> string TStatement<T>::dumpItemData () {
	std::ostringstream ret ;
	if ( qualifiers ) ret << qualifiers->dumpItemData() ;
	return ret.str() ;
}
*/

template <class T> void addStatementOrQualifiers ( T &base , TItem &item , MyJSON *m , MyJSON *qual ) {
	TPropertyNumber p = (*m)[1].i ;

	string m2 ;
	if ( m->size() > 2 ) m2 = ( (*m)[2].s ) ;
	
	TQualifiers *qs = NULL ;
	if ( qual != NULL ) {
		qs = new TQualifiers ;
		for ( uint32_t x = 0 ; x < qual->size() ; x++ ) {
			addStatementOrQualifiers <TQualifiers> ( *qs , item , &(qual->a[x]) , NULL ) ;
//			qs->addStatementOrQualifiers ( item , &(qual->a[x]) ) ;
		}
	}
	
	if ( m->size() > 2 && m2 == "bad" ) {
		if ( qs ) delete qs ;
		return ;
	} else if ( m->size() > 3 && m2 == "string" ) {
		if ( !*((*m)[2].s) ) return ; // Paranoia
		TStatement <TString> n ( item , TString ((*m)[3].s) , qs ) ;
		base.strings.addEntry ( p , n ) ;
	} else if ( m->size() > 3 && (*m)[3].has("numeric-id") ) {
		TItem qb = (*m)[3]["numeric-id"].i ;
		TStatement <TItem> n ( item , qb , qs ) ;
		base.props.addEntry ( p , n ) ;
	} else if ( m->size() > 3 && m2 == "time" ) {

		TTime t ;
		if ( !t.parseString ( (*m)[3]["time"].s ) ) { // Bad date!
			if ( qs ) delete qs ;
			return ;
		}
		t.timezone = (*m)[3]["timezone"].i ;
		t.before = (*m)[3]["before"].i ;
		t.after = (*m)[3]["after"].i ;
		t.precision = (*m)[3]["precision"].i ;

		char *e ;
		for ( e = (*m)[3]["calendarmodel"].s ; *e != 'Q' ; e++ ) ; // Extracting from URL
		t.model = atol((e+1)) ;
		
		TStatement <TTime> n ( item , t , qs ) ;
		base.times.addEntry ( p , n ) ;

	} else if ( m->size() > 3 && m2 == "globecoordinate" ) {
	
		TCoordinate coord ;
		coord.latitude = (*m)[3]["latitude"].f ;
		coord.longitude = (*m)[3]["longitude"].f ;
		coord.altitude = (*m)[3]["altitude"].f ;
		coord.has_altitude = 1 ; // TODO
		coord.precision = (*m)[3]["precision"].i ;
		
		char *e ;
		for ( e = (*m)[3]["globe"].s ; *e != 'Q' ; e++ ) ; // Extracting from URL
		coord.globe = atol((e+1)) ;

		TStatement <TCoordinate> n ( item , coord , qs ) ;
		base.coords.addEntry ( p , n ) ;
		
		
	} else if ( m->size() > 3 && m2 == "quantity" ) {

		TQuantity quant ;
		quant.amount = atof((*m)[3]["amount"].s) ;
		quant.lowerBound = atof((*m)[3]["lowerBound"].s) ;
		quant.upperBound = atof((*m)[3]["upperBound"].s) ;
		quant.unit = atof((*m)[3]["unit"].s) ;
		
		TStatement <TQuantity> n ( item , quant , qs ) ;
		base.quantities.addEntry ( p , n ) ;

	} else {
	
		string m0 ( (*m)[0].s ) ;
		if ( m0 == "novalue" ) {
			TStatement <TItem> n ( item , NOVALUE , qs ) ;
			base.props.addEntry ( p , n ) ;
		} else if ( m0 == "somevalue" ) {
			TStatement <TItem> n ( item , SOMEVALUE , qs ) ;
			base.props.addEntry ( p , n ) ;
		} else {
			cerr << "UNKNOWN TYPE " << m2 << endl ;
		}
	}
}


template <class T> TStatementList<T>::~TStatementList () {
	if ( no_qualifier_nuke ) return ; // TODO this is ... not elegant
	for ( uint32_t a = 0 ; a < list.size() ; a++ ) {
		for ( uint32_t b = 0 ; b < list[a].size() ; b++ ) {
			list[a][b].deleteQualifiers() ;
		}
	}
}


template <class T> TStatement<T>::TStatement ( TItem new_item , T new_value , TQualifiers *qs ) {
	item = new_item.item ; 
	value = new_value ; 
	qualifiers = qs ; 
	rank = RANK_DEFAULT ; 
	if ( qs ) qs->referenced++ ;
}

template <class T> TStatement<T>::TStatement ( TItemNumber new_item , T new_value , TQualifiers *qs ) {
	item = new_item ;
	value = new_value ;
	qualifiers = qs ;
	rank = RANK_DEFAULT ;
	if ( qs ) qs->referenced++ ;
}

template <class T> void TStatement<T>::setFrom ( TStatement <T> &other ) {
	item = other.item ;
	value = other.value ;
	rank = other.rank ;
	if ( other.qualifiers ) {
		qualifiers = new TQualifiers ;
		*qualifiers = *(other.qualifiers) ;
	} else qualifiers = NULL ;
}

template <class T> TStatement<T>::~TStatement () {
}

template <class T> void TStatement<T>::deleteQualifiers() {
	if ( !qualifiers ) return ;
	qualifiers->referenced-- ;
	if ( qualifiers->referenced > 0 ) return ;
	delete qualifiers ;
//	qualifiers = NULL ;
}

template <class T> void TStatement<T>::writeBinary ( FILE *out ) {
	FWRITE(item);
	value.writeBinary ( out ) ;
	
	uint8_t x = rank ;
	if ( qualifiers != NULL ) x |= HAS_QUALIFIERS ;
	FWRITE(x);
	if ( x&HAS_QUALIFIERS ) qualifiers->writeBinary ( out ) ;
}

template <class T> void TStatement<T>::readBinary ( FILE *in , uint8_t major_version , uint8_t minor_version ) {
	FREAD(item);
	value.readBinary ( in , major_version , minor_version ) ;
	
	if ( major_version < 2 ) return ;
	if ( major_version == 2 && minor_version < 3 ) return ; // >= V2.3
	FREAD(rank);
	if ( rank&HAS_QUALIFIERS ) {
		rank -= HAS_QUALIFIERS ;
		qualifiers = new TQualifiers ;
		qualifiers->readBinary ( in , major_version , minor_version ) ;
	}
}

//________________________________________________________________________________________________


bool itempair_is_greater ( TStatement <TItem> i1 , TStatement <TItem> i2 ) {
	return ( i1.item == i2.item ) ? (( i1.value.isDummy() || i2.value.isDummy() )?false:( i1.value.item > i2.value.item )) : ( i1.item > i2.item ) ;
}

TItemSet::TItemSet () {
	busy = true ;
	query_count = 0 ;
	props.sorter = itempair_is_greater ;
	rprops.sorter = itempair_is_greater ;
}


//________________________________________________________________________________________________

void split ( char sep , char *s , vector <char *> &vc ) {
	vc.clear() ;
	char *lc = s ;
	for ( char *c = s ; *c ; c++ ) {
		if ( *c == sep ) {
			*c = 0 ;
			vc.push_back ( lc ) ;
			lc = c+1 ;
		}
	}
	if ( *lc ) vc.push_back ( lc ) ;
}

void escapeJsonStringToStringStream(const std::string& input,std::ostringstream &ss) {
//    std::ostringstream ss;
//    for (auto iter = input.cbegin(); iter != input.cend(); iter++) {
    //C++98/03:
    for (std::string::const_iterator iter = input.begin(); iter != input.end(); iter++) {
        switch (*iter) {
            case '\\': ss << "\\\\"; break;
            case '"': ss << "\\\""; break;
            case '/': ss << "\\/"; break;
            case '\b': ss << "\\b"; break;
            case '\f': ss << "\\f"; break;
            case '\n': ss << "\\n"; break;
            case '\r': ss << "\\r"; break;
            case '\t': ss << "\\t"; break;
            default: ss << *iter; break;
        }
    }
//    return ss.str();
}

string escapeJsonStringToStringStream(const std::string& input) {
	std::ostringstream ss ;
	escapeJsonStringToStringStream ( input , ss ) ;
	return ss.str() ;
}

void myReplace(std::string& str, const std::string& oldStr, const std::string& newStr)
{
  size_t pos = 0;
  while((pos = str.find(oldStr, pos)) != std::string::npos)
  {
     str.replace(pos, oldStr.length(), newStr);
     pos += newStr.length();
  }
}

//________________________________________________________________________________________________


/**
	Returns true if parsing went OK. Allows for "short" dates/times.
*/
bool TTime::parseString ( char *s ) {
	// +00000001861-03-17T00:00:00Z
	year = 0 ;
	timezone = before = after = 0 ;
	month = day = hour = minute = second = precision = 0 ;
	model = 0 ;
	is_initialized = true ;
	bool end = false ;
	
	char *d = s ;
	if ( *d == '+' ) d++ ;
	char *ld = d ;
	for ( ; *d && *d != '-' ; d++ ) ;
	if ( !*d ) end = true ;
	*d++ = 0 ; year = atoll ( ld ) ;
	ld = d ;
	if ( end ) return true ;
	for ( d++ ; *d && *d != '-' ; d++ ) ;
	if ( !*d ) end = true ;
	*d++ = 0 ; month = atoi ( ld ) ;
	ld = d ;
	if ( end ) return true ;
	for ( d++ ; *d && *d != 'T' ; d++ ) ;
	if ( !*d ) end = true ;
	*d++ = 0 ; day = atoi ( ld ) ;
	ld = d ;
	if ( end ) return true ;
	for ( d++ ; *d && *d != ':' ; d++ ) ;
	if ( !*d ) end = true ;
	*d++ = 0 ; hour = atoi ( ld ) ;
	ld = d ;
	if ( end ) return true ;
	for ( d++ ; *d && *d != ':' ; d++ ) ;
	if ( !*d ) end = true ;
	*d++ = 0 ; minute = atoi ( ld ) ;
	ld = d ;
	if ( end ) return true ;
	for ( d++ ; *d && *d != 'Z' ; d++ ) ;
	if ( !*d ) end = true ;
	*d++ = 0 ; second = atoi ( ld ) ;
	ld = d ;
	if ( end ) return true ;
	// microseconds???
	return true ;
}

string TTime::toString () {
	if ( !is_initialized ) return "" ;
	char t[100] ; // +00000001861-03-17T00:00:00Z
	sprintf ( t , "%c%011ld-%02d-%02dT%02d:%02d:%02dZ" , (year<0?'-':'+') , (long) abs(year) , month , day , hour , minute , second ) ; // ld or lld?
	return t ;
} ;


//________________________________________________________________________________________________

string TCoordinate::toString () {
	char tmp[1000] ;
	if ( has_altitude ) sprintf ( tmp , "%f|%f|%f|%d" , latitude , longitude , altitude , precision ) ;
	else sprintf ( tmp , "%f|%f||%d" , latitude , longitude , precision ) ;
	return string ( tmp ) ;
}


//________________________________________________________________________________________________

string TQuantity::toString () {
	char tmp[1000] ;
	sprintf ( tmp , "%f|%f|%f|%f" , amount , lowerBound , upperBound , unit ) ;
	return string ( tmp ) ;
}



//________________________________________________________________________________________________

string TItemSet::dumpItemData ( TItem i ) {
	string ret ;
	ret += props.dumpItemData ( i ) ;
	ret += strings.dumpItemData ( i ) ;
	ret += coords.dumpItemData ( i ) ;
	ret += times.dumpItemData ( i ) ;
	ret += quantities.dumpItemData ( i ) ;
	return ret ;
}

void TItemSet::clear () {
	props.clear() ;
	rprops.clear() ;
	strings.clear() ;
	times.clear() ;
	quantities.clear() ;
	coords.clear() ;
	links.clear() ;
	m_item.clear() ;
	m_prop.clear() ;
	update_files.clear() ;
}

//____

void TItemSet::importBinary ( string fn , bool just_time ) {
	FILE *in = fopen(fn.c_str(), "rb");
	importBinary ( in , just_time ) ;
	fclose ( in ) ;
}


void TItemSet::importBinary ( FILE *in , bool just_time ) {
	uint32_t num_props ;
	char buffer[BUFSIZE] ;
	
	clear() ;

	// Version
	uint8_t major_version = 0 , minor_version = 0 ;
	FREAD(major_version) ;
	
	
	if ( major_version > 2 ) return ; // Version newer than code

	if ( major_version > 1 ) {
		FREAD(minor_version) ;

		char *c ;
		fgets(buffer,BUFSIZE,in) ;
		for ( c = buffer ; *c && *c != '\n' ; c++ ) ;
		*c = 0 ;
		time_start = buffer ;

		fgets(buffer,BUFSIZE,in) ;
		for ( c = buffer ; *c && *c != '\n' ; c++ ) ;
		*c = 0 ;
		time_end = buffer ;
	}

	if ( just_time ) return ;
	
	
	// Parts

	props.readBinary ( in , major_version , minor_version ) ;
	strings.readBinary ( in , major_version , minor_version ) ;
	times.readBinary ( in , major_version , minor_version ) ;
	coords.readBinary ( in , major_version , minor_version ) ;

	// Links
	if ( major_version > 2 || ( major_version == 2 && minor_version > 0 ) ) {
		FREAD(num_props) ;
		
		for ( uint32_t a = 0 ; a < num_props ; a++ ) {
		
			fgets(buffer,BUFSIZE,in) ;
			char *c ;
			for ( c = buffer ; *c && *c != '\n' ; c++ ) ;
			*c = 0 ;
			string wiki ( buffer ) ;
			
			uint32_t num_items ;
			fread(&num_items, sizeof(num_items), 1, in);
			links[wiki].resize ( num_items ) ;
			for ( uint32_t b = 0 ; b < num_items ; b++ ) {
				FREAD(links[wiki][b]) ;
			}
			
		}
	}


	if ( major_version > 2 || ( major_version == 2 && minor_version > 1 ) ) {
		quantities.readBinary ( in , major_version , minor_version ) ;
	}

	prepareProps () ;
	
//	cerr << getStatsString() ;
}

void TItemSet::exportBinary ( string fn ) {
	FILE* out = fopen(fn.c_str(), "wb+");
	exportBinary ( out ) ;
	fclose ( out ) ;
}



void TItemSet::exportBinary ( FILE *out ) {
	char nl = '\n' ;
	
	// Version
	uint8_t major_version = 2 , minor_version = 4 ;
	FWRITE(major_version);
	FWRITE(minor_version) ;

	// Time
	const char *s ;
	s = time_start.c_str() ;
	fwrite(s, sizeof(char), strlen(s), out);
	fwrite(&nl,sizeof(char),1,out);
	s = time_end.c_str() ;
	fwrite(s, sizeof(char), strlen(s), out);
	fwrite(&nl,sizeof(char),1,out);

	// Parts
	props.writeBinary ( out ) ;
	strings.writeBinary ( out ) ;
	times.writeBinary ( out ) ;
	coords.writeBinary ( out ) ;
	
	
	// Links
	uint32_t num_props = links.size() ;
	FWRITE(num_props) ;
	for ( Tmsvi::iterator i = links.begin() ; i != links.end() ; i++ ) {
		const char *s = i->first.c_str() ;
		fwrite(s, sizeof(char), strlen(s), out);
		fwrite(&nl,sizeof(char),1,out);
		uint32_t l = i->second.size() ;
		FWRITE(l) ;
		for ( uint32_t a = 0 ; a < l ; a++ ) {
			FWRITE(i->second[a]) ;
		}
	}
	
	quantities.writeBinary ( out ) ;
}

TPropertyNumber TItemSet::getMaxProp () {
	TPropertyNumber max = 0 ;
	if ( props.size() > max ) max = props.size() ;
	if ( strings.size() > max ) max = strings.size() ;
	if ( times.size() > max ) max = times.size() ;
	if ( coords.size() > max ) max = coords.size() ;
	return max ;
}


string TItemSet::getLabel ( map <TItem,TLabelPair> &m , TItem i , string lang ) {
	if ( m.find(i) == m.end() ) return "" ;
	if ( m[i].label.find ( lang ) == m[i].label.end() ) return "" ;
	return m[i].label[lang] ;
}


//typedef pair <TItem,TItem> itempair ;
//typedef vector <itempair> Tvsi ;

// TODO this could be more efficient...
void TItemSet::getPropJSONForItems ( TPropertyNumber p , vector <TItem> &items , string &s , bool add_target_items ) {
	char tmp[10000] ;
	map <TItem,bool> targets ;
	
	addOutputStrings ( p , items , s , add_target_items?&targets:NULL ) ;

	if ( !add_target_items ) return ;

	// items is already sorted by addOutputStrings
	std::vector<TItem>::iterator it;
	it = std::unique (items.begin(), items.end());
	items.resize( std::distance(items.begin(),it) );
}

void TItemSet::setBusy ( bool b ) {
	// TODO : When setting to true, wait until all other processes have ended before returning
	busy = b ;
}

void TItemSet::fixRprops () {
	rprops.resize ( props.size() ) ;
	for ( uint32_t p = 0 ; p < props.size() ; p++ ) {
		rprops[p].clear() ;
		rprops[p].reserve ( props[p].size() ) ;
		for ( vector<TStatement<TItem> >::iterator i = props[p].begin() ; i != props[p].end() ; i++ ) {
			rprops[p].push_back ( TStatement<TItem> ( i->value.item , i->item ) ) ;
		}
	}
	rprops.sortAll() ;
}



void TItemSet::mergeItemLists ( vector <TItem> &r1 , vector <TItem> &r2 , vector <TItem> &r ) {
	uint32_t i1 = 0 , i2 = 0 ;
	r.reserve ( r1.size() + r2.size() ) ; // Result cannot be larger than both sub-results together
	while ( i1 < r1.size() || i2 < r2.size() ) {
		if ( i1 == r1.size() ) r.push_back ( r2[i2++] ) ;
		else if ( i2 == r2.size() ) r.push_back ( r1[i1++] ) ;
		else if ( r1[i1] < r2[i2] ) r.push_back ( r1[i1++] ) ;
		else if ( r1[i1] > r2[i2] ) r.push_back ( r2[i2++] ) ;
		else { // Same
			r.push_back ( r1[i1] ) ;
			i1++ ;
			i2++ ;
		}
	}
}


void TItemSet::mergeFromUpdate ( TItemSet &w ) {

	// This updates from w, assuming it is more up-to-date

	map <TItemNumber,bool> remove ;
	w.props.getAllItems ( remove ) ;
	w.strings.getAllItems ( remove ) ;
	w.coords.getAllItems ( remove ) ;
	w.times.getAllItems ( remove ) ;
	w.quantities.getAllItems ( remove ) ;
	
	setBusy ( true ) ;
	props.updateFrom ( w.props , remove ) ;
	strings.updateFrom ( w.strings , remove ) ;
	coords.updateFrom ( w.coords , remove ) ;
	times.updateFrom ( w.times , remove ) ;
	quantities.updateFrom ( w.quantities , remove ) ;
	
	// Links
	for ( Tmsvi::iterator i = links.begin() ; i != links.end() ; i++ ) links[i->first].size() ;
	for ( Tmsvi::iterator i = w.links.begin() ; i != w.links.end() ; i++ ) links[i->first].size() ;

	for ( Tmsvi::iterator i = links.begin() ; i != links.end() ; i++ ) {
		links[i->first].size() ;
		w.links[i->first].size() ;
		vector <TItem> r ;
		mergeItemLists ( links[i->first] , w.links[i->first] , r ) ;
		r.swap ( links[i->first] ) ;
//		cerr << i->first << " : " << links[i->first].size() << " + " << w.links[i->first].size() << " = " << links[i->first].size() << endl ;
	}
	
	
	if ( !w.time_end.empty() ) time_end = w.time_end ;
	prepareProps() ;
	
	setBusy ( false ) ;
	cerr << getStatsString() ;
}

void TItemSet::resetFromItemset ( TItemSet &i ) {
	props.swap ( i.props ) ;
	rprops.swap ( i.rprops ) ;
	strings.swap ( i.strings ) ;
	times.swap ( i.times ) ;
	coords.swap ( i.coords ) ;
	quantities.swap ( i.quantities ) ;
	links.swap ( i.links ) ;
}

bool TItemSet::updateFromFile ( string filename , bool is_binary ) {
	if ( update_files.find(filename) != update_files.end() ) return false ;
	update_files[filename] = true ;

	TItemSet w ;
	
	if ( is_binary ) {
		w.importBinary ( filename ) ;
	} else {
		FILE *file = fopen ( filename.c_str() , "r" ) ;
		w.import_item_connections ( file , false ) ;
		fclose ( file ) ;
	}

	mergeFromUpdate ( w ) ;
	return true ;
}

void TItemSet::import_binary_fofn ( bool reset ) {
	ifstream in ( binary_fofn.c_str() );
	uint32_t row = 0 ;
	string filename ;
	while ( getline ( in , filename ) ) {
		if ( reset && row == 0 ) {
			TItemSet tmp ;
			tmp.importBinary ( filename ) ;
			setBusy ( true ) ;
			resetFromItemset ( tmp ) ;
			time_start = tmp.time_start ;
			time_end = tmp.time_end ;
			setBusy ( false ) ;
		} else updateFromFile ( filename , true ) ;
		row++ ;
	}
	in.close() ;
}



void TItemSet::import_xml_dump ( FILE *file , bool initial_import ) {
	char buffer[BUFSIZE] , *c , *d ;
	setBusy ( true ) ;
	while ( query_count > 0 ) usleep ( 100 * 1000 ) ; // Wait 0.1sec for queries to end, then try again

	string model , title , ts , text ;
	TItem item ;
	const TItemNumber empty_item = NOITEM ;

	while ( fgets ( buffer , BUFSIZE , file ) ) {
		if ( !*buffer ) break ; // Paranoia

		for ( c = buffer ; *c == ' ' || *c == 9 ; c++ ) ;
		if ( *c != '<' ) continue ; // Not XML
		
		bool closing = false ;
		if ( *(c+1) == '/' ) {
			c++ ;
			closing = true ;
		}
		
		string tag ;
		for ( c++ ; *c && *c != '>' && *c != ' ' ; c++ ) tag += *c ;
		for ( ; *c && *c != '>' ; c++ ) ; // Skip attributes ;
		if ( !*c ) continue ; // Premature line end
		
		if ( closing && tag == "page" ) {
			if ( text.empty() ) continue ;
			
			
			if ( item != empty_item && model == "wikibase-item" ) { // Items only
				myReplace ( text , "&quot;" , "\"" ) ;
				string tmp = text + " " ;
				MyJSON j ( (char*)text.c_str() ) ;
				
				// TODO : Labels

				if ( j.has("links") ) {
					for ( map <string,MyJSON>::iterator i = j["links"].o.begin() ; i != j["links"].o.end() ; i++ ) {
						links[i->first].push_back ( item ) ;
					}
				}

				if ( j.has("claims") ) {
					for ( uint32_t a = 0 ; a < j["claims"].size() ; a++ ) {

						if ( !j["claims"][a].has("m") ) continue ;
						MyJSON *m = &(j["claims"][a]["m"]) ;
						if ( m->size() < 2 ) continue ;

						MyJSON *qual = NULL ;
						if ( j["claims"][a]["q"].size() > 0 ) qual = &(j["claims"][a]["q"]) ;
						addStatementOrQualifiers ( *this , item , m , qual ) ;
						
					}
				}

				
			}
			
			title.clear() ;
			text.clear() ;
			continue ;
		}

		
		char *content_begin = ++c ;
		char *content_end = NULL ;
		
		for ( ; *c ; c++ ) {
			if ( *c == '<' && *(c+1) == '/' ) content_end = c ; // Last 
		}

		if ( tag == "timestamp" ) {
			*content_end = 0 ;
			string s ( content_begin ) ;
			if ( time_start.empty() || time_start > s ) time_start = s ;
			if ( time_end.empty() || time_end < s ) time_end = s ;
			continue ;
		}

		if ( !content_end ) continue ; // No closing tag
		
		*content_end = 0 ;
		string content = content_begin ;
		
		if ( tag == "model" ) {
			model = content ;
		} else if ( tag == "title" ) {
			title = content ;
			content_begin = strchr(content_begin , 'Q');
			item = content_begin ? atol ( content_begin+1 ) : NOITEM ;
		} else if ( tag == "text" ) {
			text = content ;
		}
		
		
	}

	if ( initial_import ) prepareProps () ;
//	cerr << getStatsString() ;
	setBusy ( false ) ;
}

void TItemSet::import_item_connections ( FILE *file , bool initial_import ) {
	char buffer[BUFSIZE] , *c , *d ;
	setBusy ( true ) ;
	while ( query_count > 0 ) usleep ( 100 * 1000 ) ; // Wait 0.1sec for queries to end, then try again

	vector <char*> vc ;
	vc.reserve ( 10 ) ;
	while ( fgets ( buffer , BUFSIZE , file ) ) {
		if ( !*buffer ) break ; // Paranoia
		for ( c = buffer ; *c != 9 ; c++ ) ;
		*c++ = 0 ;
		
		// Item ID
		TItem qa ;
		bool is_prop = false ;
		if ( *buffer == 'P' ) {
			is_prop = true ;
			qa = atol ( buffer+1 ) ;
		}
		else qa = atol ( buffer ) ;
		
		// Property or label
		for ( d = c ; *c != 9 ; c++ ) ;
		*c++ = 0 ;
		
		if ( *d == 'L' && *(d+1) == ':' ) { // Label
		continue ; // HACK FIXME Don't load labels - too much memory ATM
			d += 2 ;
			string wiki = d ;
			for ( d = c ; *c && *c != 10 && *c != 13 ; c++ ) ;
			*c = 0 ;
			string label = d ;
			if ( is_prop ) m_prop[qa].label[wiki] = label ;
			else m_item[qa].label[wiki] = label ;
			continue ;
		}
		
		uint32_t p = atol ( d ) ;
		
		if ( *c == 'S' ) { // String
			for ( d = c ; *c && *c != 10 && *c != 13 ; c++ ) ;
			*c = 0 ;
			string s = (d+1) ;
			if ( s.empty() ) continue ; // Paranoia
			if ( p >= strings.size() ) strings.resize ( p+1 ) ;
			strings[p].push_back ( TStatement <TString> ( qa , s ) ) ;

		} else if ( *c == 'T' ) { // Time
			split ( '|' , c+2 , vc ) ;
			if ( vc.size() != 6 ) { cerr << buffer << endl ; exit ( 0 ) ; } // Paranoia
			TTime t ;
			if ( !t.parseString ( vc[0] ) ) continue ; // Bad date!
			t.timezone = atoi ( vc[1] ) ;
			t.before = atoi ( vc[2] ) ;
			t.after = atoi ( vc[3] ) ;
			t.precision = atoi ( vc[4] ) ;
			t.model = atol ( vc[5] ) ;
			if ( p >= times.size() ) times.resize ( p+1 ) ;
			times[p].push_back ( TStatement <TTime> ( qa , t ) ) ;

		} else if ( *c == 'C' ) { // Coordinate
			split ( '|' , c+2 , vc ) ;
			if ( vc.size() != 5 ) { cerr << vc[0] << endl ; exit ( 0 ) ; } // Paranoia
			TCoordinate coord ;
			coord.latitude = atof ( vc[0] ) ;
			coord.longitude = atof ( vc[1] ) ;
			coord.altitude = atof ( vc[2] ) ;
			coord.has_altitude = (vc[2][0] != 0) ;
			coord.precision = atoi ( vc[3] ) ;
			coord.globe = atol ( vc[4] ) ;
			if ( p >= coords.size() ) coords.resize ( p+1 ) ;
			coords[p].push_back ( TStatement <TCoordinate> ( qa , coord ) ) ;
			

		} else { // Item connection
			for ( d = c ; *c ; c++ ) ;
			TItem qb = atol ( d ) ;
			if ( p >= props.size() ) props.resize ( p+1 ) ;
			props[p].push_back ( TStatement <TItem> ( qa , qb ) ) ;
		}
	}
	
	if ( initial_import ) prepareProps () ;
	cerr << getStatsString() ;
	setBusy ( false ) ;
}


void TItemSet::prepareProps() {
	strings.sortAll() ;
	times.sortAll() ;
	quantities.sortAll() ;
	coords.sortAll() ;
	props.sortAll() ;

	for ( Tmsvi::iterator i = links.begin() ; i != links.end() ; i++ )
		sort ( i->second.begin() , i->second.end() ) ;

	fixRprops() ;
}

string TItemSet::getStatsString() {
	std::ostringstream ret ;
	uint32_t total = 0 ;
	ret << strings.getTotalCount() << " strings" << endl ;

	total = 0 ;
	ret << times.getTotalCount() << " times" << endl ;

	ret << coords.getTotalCount() << " coordinates" << endl ;

	total = 0 ;
	for ( uint32_t a = 0 ; a < props.size() ; a++ ) {
		total += props[a].size() ;
	}
	ret << total << " connections" << endl ;

	ret << quantities.getTotalCount() << " quantities" << endl ;
	
	total = 0 ;
	for ( Tmsvi::iterator i = links.begin() ; i != links.end() ; i++ ) {
		total += i->second.size() ;
	}
	ret << total << " links" << endl ;
	
	
	ret << "Times : " << time_start << " - " << time_end << endl ;
	return ret.str() ;
}

void TItemSet::writeClaims ( string filename ) {
	ofstream out ( filename.c_str() ) ;
	for ( uint32_t p = 0 ; p < props.size() ; p++ ) {
		for ( Tvsi::iterator i = props[p].begin() ; i != props[p].end() ; i++ ) {
		out << i->item << "\t" << p << "\t" << i->value.item << endl ;
		}
	}
}

Tmsvi TItemSet::getMultipleStrings ( TPropertyNumber p ) {
	Tmsvi ret , tmp ;
	if ( p >= strings.size() ) return ret ; // Out-of-bounds
	
	for ( uint32_t a = 0 ; a < strings[p].size() ; a++ ) {
		tmp[strings[p][a].value.s].push_back ( strings[p][a].item ) ;
	}
	
	for ( Tmsvi::iterator i = tmp.begin() ; i != tmp.end() ; i++ ) {
		if ( i->second.size() == 1 ) continue ;
		ret[i->first] = i->second ;
	}
	
	return ret ;
}


Tvsi TItemSet::getMissingPairs ( TPropertyNumber p1 , vector <TPropertyNumber> &pl2 ) {
	Tvsi ret ;
	if ( p1 >= props.size() ) return ret ; // Out-of-bounds
	for ( uint32_t a = 0 ; a < pl2.size() ; a++ ) {
		if ( pl2[a] >= props.size() ) return ret ; // Out-of-bounds
	}
	
	typedef pair <TItemNumber,TItemNumber> itempair ;
	map <itempair,uint32_t> count ;
	for ( uint32_t a = 0 ; a < props[p1].size() ; a++ ) {
		if ( props[p1][a].value.item == NOVALUE || props[p1][a].value.item == SOMEVALUE ) continue ;
		count[itempair(props[p1][a].item,props[p1][a].value.item)] = 0 ;
	}

	for ( vector <TPropertyNumber>::iterator p2 = pl2.begin() ; p2 != pl2.end() ; p2++ ) {
		for ( vector < TStatement <TItem> >::iterator ii = props[*p2].begin() ; ii != props[*p2].end() ; ii++ ) {
			itempair ip (ii->value.item,ii->item) ;
			if ( count.find(ip) != count.end() ) count[ip]++ ;
		}
	}
	
	ret.reserve ( count.size() ) ;
	for ( map<itempair,uint32_t>::iterator i = count.begin() ; i != count.end() ; i++ ) {
		if ( i->second > 0 ) continue ;
		TStatement <TItem> tmp ;
		tmp.item = i->first.first ;
		tmp.value.item = i->first.second ;
		ret.push_back ( tmp ) ;
	}
	
	return ret ;
}



void TItemSet::findItemsWithoutLink ( vector <string> &has_link , TIntermediateResult &hadthat ) {
	for ( Tmsvi::iterator i = links.begin() ; i != links.end() ; i++ ) {
		bool found = false ;
		for ( uint32_t a = 0 ; a < has_link.size() && !found ; a++ ) {
			if ( has_link[a] == i->first ) found = true ;
		}
		if ( found ) continue ;
		findItemsWithLink ( i->first , hadthat ) ;
	}
	
	for ( uint32_t a = 0 ; a < has_link.size() ; a++ ) {
		findItemsWithLink ( has_link[a] , hadthat , true ) ;
	}
}

void TItemSet::findItemsWithLink ( string project , TIntermediateResult &hadthat , bool remove ) {
	if ( links.find(project) == links.end() ) return ;
	for ( uint32_t b = 0 ; b < links[project].size() ; b++ ) {
		if ( remove ) {
			if ( hadthat.hasItem(links[project][b]) ) hadthat.removeItem(links[project][b]) ;
		} else hadthat.addItem(links[project][b]) ;
	}
}

void TItemSet::followChains ( TItem &item , bool forward , TIntermediateResult &hadthat , vector <TPropertyNumber> &follow_forward , vector <TPropertyNumber> &follow_reverse ) {
	map <TItem,bool> last , next ;
	
	// Init
	last[item] = true ;
	
	// Iterate
	while ( last.size() > 0 ) {
		for ( map <TItem,bool>::iterator i = last.begin() ; i != last.end() ; i++ ) {
			if ( i->first.item != NOVALUE && i->first.item != SOMEVALUE ) {
				hadthat.addItem(i->first) ;
			}
		}
		next.clear() ;
		
		if ( forward ) {
			for ( uint32_t pid = 0 ; pid < follow_forward.size() ; pid++ ) {
				TPropertyNumber p = follow_forward[pid] ;
				if ( p >= props.size() ) continue ; // Out-of-bounds
				for ( map <TItem,bool>::iterator i = last.begin() ; i != last.end() ; i++ ) {
					TStatement <TItem> lookup ( i->first , TItem(0) ) ;
					props.markEqualRange ( p , lookup , hadthat , next ) ;
				}
			}
		} else {
			for ( uint32_t pid = 0 ; pid < follow_reverse.size() ; pid++ ) {
				TPropertyNumber p = follow_reverse[pid] ;
				if ( p >= rprops.size() ) continue ; // Out-of-bounds
				for ( map <TItem,bool>::iterator i = last.begin() ; i != last.end() ; i++ ) {
					TStatement <TItem> lookup ( i->first , TItem(0) ) ;
					rprops.markEqualRange ( p , lookup , hadthat , next ) ;
				}
			}
		}

		last = next ;
	}

}

	
void TItemSet::followWeb ( TIntermediateResult &had , TIntermediateResult &hadthat , vector <TPropertyNumber> &follow_forward , vector <TPropertyNumber> &follow_reverse ) {
	map <TItem,bool> last , next ;
	
	// Init
	had.setMap ( last ) ;
//	for ( map <TItem,bool>::iterator i = had.begin() ; i != had.end() ; i++ ) last[i->first] = true ;
	
	// Iterate
	while ( last.size() > 0 ) {
		for ( map <TItem,bool>::iterator i = last.begin() ; i != last.end() ; i++ ) {
			if ( i->first.item != NOVALUE && i->first.item != SOMEVALUE ) hadthat.addItem(i->first) ;
		}
		next.clear() ;
		
		for ( uint32_t pid = 0 ; pid < follow_forward.size() ; pid++ ) {
			TPropertyNumber p = follow_forward[pid] ;
			if ( p >= props.size() ) continue ; // Out-of-bounds
			for ( map <TItem,bool>::iterator i = last.begin() ; i != last.end() ; i++ ) {
				TStatement <TItem> lookup ( i->first , TItem(0) ) ;
				props.markEqualRange ( p , lookup , hadthat , next ) ;
			}
		}

		for ( uint32_t pid = 0 ; pid < follow_forward.size() ; pid++ ) {
			TPropertyNumber p = follow_forward[pid] ;
			if ( p >= rprops.size() ) continue ; // Out-of-bounds
			for ( map <TItem,bool>::iterator i = last.begin() ; i != last.end() ; i++ ) {
				TStatement <TItem> lookup ( i->first , TItem(0) ) ;
				rprops.markEqualRange ( p , lookup , hadthat , next ) ;
			}
		}
		
		last = next ;
	}
}

void TItemSet::removeItems ( vector <TItemNumber> &items ) {
	props.removeItems ( items ) ;
	strings.removeItems ( items ) ;
	coords.removeItems ( items ) ;
	times.removeItems ( items ) ;
	quantities.removeItems ( items ) ;
	vector <TItem> items2 ;
	for ( uint32_t a = 0 ; a < items.size() ; a++ ) items2.push_back ( TItem ( items[a] ) ) ;
	props.removeValues ( items2 ) ;
	fixRprops() ;
}
